# TESTBED for Django in GitLab Auto DevOps

## Setup

### Kubernetes
- At Operations -> Kubernetes add a Cluster.
- After the cluster is created install the applications
    
    1. Prometheus (To see some infos in the Health tab)
    2. Cert-Manager because you want HTTPS
    3. Ingress to serve you page to the web

- After installing all 3 Applications go back to Details tab and add the external IP/Domain. 
If you have no IP use the recommanded 123.123.123.123.nip.io from the help test. The IP is already right in the help text.

### Optional DNS
If you want to server your page on a Domain, you have to add 2 A records.

```
domain.de. 0	A	123.123.123.123	
*.daomain.de. 0	A	123.123.123.123	
```
Also add the Variable PRODUCTION_ADDITIONAL_HOSTS with the value of domain.de

### Variables
If cloned add Settings -> CI / CD -> Add Variables:

TEST_DISABLED with value true to disable not working (with django) auto test.

K8S_SECRET_SECRETKEY to set the Djangos SECRET_KEY 

